

import theano
import theano.tensor as T

from numba import vectorize, guvectorize, float32, float64, int32, int64
import numpy as np
import scipy as sp


import numexpr as nexp

a = np.matrix(np.random.rand(300,300)).astype(dtype=np.float32)
b = np.matrix(np.random.rand(300,200)).astype(dtype=np.float32)
c = sp.copy(a)

@vectorize([int32(int32, int32),
            int64(int64, int64),
            float32(float32, float32),
            float64(float64, float64)], nopython = True)
def f(x, y):
    return x + y


@vectorize([int32(int32, int32),
            int64(int64, int64),
            float32(float32, float32),
            float64(float64, float64)], nopython = False)
def fmult(x, y):
    return np.cross(a,b)


@guvectorize(['int32[:,:], int32[:,:], int32[:,:]',
              'int64[:,:], int64[:,:], int64[:,:]',
              'float32[:,:], float32[:,:], float32[:,:]',
              'float64[:,:], float64[:,:], float64[:,:]'],'(m,n), (n,p) -> (m,p)', target='cpu')
def fmult_gu(a, b, c):
    c = np.multiply(a, b)
    return c


@guvectorize(['int32[:,:], int32[:,:], int32[:,:]',
              'int64[:,:], int64[:,:], int64[:,:]',
              'float32[:,:], float32[:,:], float32[:,:]',
              'float64[:,:], float64[:,:], float64[:,:]'],'(m,n), (n,p) -> (m,p)', target='cpu')
def fdot_gu(a, b, c):
    c = np.dot(a, b)
    return c



@vectorize([float32(float32,float32)])
def mult_matrix(a,b):
    return np.multiply(a, b)


@guvectorize(['int32[:,:], int32[:,:], int32[:,:]',
              'int64[:,:], int64[:,:], int64[:,:]',
              'float32[:,:], float32[:,:], float32[:,:]',
              'float64[:,:], float64[:,:], float64[:,:]'],'(m,n), (n,p) -> (m,p)', target='cpu')
def matmulcore(A, B, C):
    m, n = A.shape
    n, p = B.shape
    for i in range(m):
        for j in range(p):
            C[i, j] = 0
            for k in range(n):
                C[i, j] += A[i, k] * B[k, j]



@guvectorize(['int32[:,:], int32[:,:], int32[:,:]',
              'int64[:,:], int64[:,:], int64[:,:]',
              'float32[:,:], float32[:,:], float32[:,:]',
              'float64[:,:], float64[:,:], float64[:,:]'],'(m,n), (n,p) -> (m,p)', target='cpu', nopython=True)
def mult_elemwise(A, B, C):
    m, n = A.shape
    for i in range(m):
        for j in range(n):
            C[i, j] = A[i, j] * B[i, j]


@guvectorize(['int32[:,:,:], int32[:,:,:], int32[:,:,:]',
              'int64[:,:,:], int64[:,:,:], int64[:,:,:]',
              'float32[:,:,:], float32[:,:,:], float32[:,:,:]',
              'float64[:,:,:], float64[:,:,:], float64[:,:,:]'],'(m,n,o), (m,n,o) -> (m,n,o)')
def mult3_elemwise(A, B, C):
    m, n, o = A.shape
    for i in range(m):
        for j in range(n):
            for k in range(o):
                C[i, j, k] = A[i, j, k] * B[i, j, k]



@guvectorize(['int32[:,:,:], int32[:,:,:], int32[:,:,:]',
              'int64[:,:,:], int64[:,:,:], int64[:,:,:]',
              'float32[:,:,:], float32[:,:,:], float32[:,:,:]',
              'float64[:,:,:], float64[:,:,:], float64[:,:,:]'],'(m,n,o), (m,n,o) -> (m,n,o)')
def mult4_elemwise(A, B, C):
    C[1:-1,1:-1,1:-1] = A[2:, 1:-1, 1:-1] + B[:-2, 1:-1, 1:-1] - 2.0*A[1:-1,1:-1,1:-1]





@vectorize(['float32[:,:,:](float32[:,:,:])'])
def heat_iter(a):
    c = a*0
    c[1:-1,1:-1,1:-1] = a[2:, 1:-1, 1:-1] + a[:-2, 1:-1, 1:-1] - 2.0 * a[1:-1, 1:-1, 1:-1]
    return c


%timeit mult_matrix(a,b)

f(a,c)

m,n = a.shape
o,p = b.shape

d = np.array((m,p)).astype(dtype=np.float32)

aa = np.matrix(np.array([[1,2,3],[4,5,6],[7,8,9]], dtype=np.float32))
bb = sp.copy(aa)

%timeit dd = fmult_gu(a, a)
dd


matmulcore(aa, aa)

%timeit np.multiply(a,a)
%timeit mult_elemwise(a, a)


a3 = np.array(np.random.rand(30,300,300), dtype=np.float32)
b3 = np.array(np.random.rand(30,300,300), dtype=np.float32)

%timeit mult4_elemwise(a3, b3)


c3 = a3*0

%timeit c3[1:-1,1:-1,1:-1] = a3[2:,1:-1,1:-1] + b3[:-2,1:-1,1:-1] - 2.0* a3[1:-1,1:-1,1:-1]




Lx = np.linspace(0,3, 4)
Ly = np.linspace(0,3, 4)
Lz = np.linspace(0,2, 3)

zz,yy,xx = np.meshgrid(Lz, Ly, Lx)

zz = zz.astype(dtype=np.int)
yy = yy.astype(dtype=np.int)
xx = xx.astype(dtype=np.int)



vv = np.array(zip(*[zz.flatten(),yy.flatten(),xx.flatten()]), dtype=np.int)
vv



import  time as tm


def handleControlC(original_func):
    def new_func(*w, **kw):
        try:
            original_func(*w,**kw)
        except KeyboardInterrupt:
            print '\n\nExicting program...\n'
            exit()

    return new_func


@handleControlC
def infLoop(val, str=""):
    while True:
        print 'val == ', val
        print 'str == ', str
        # tm.sleep(0.1)



infLoop(100, str="Marzuk")

import numpy as np

zs = 3
ys = 5
xs = 6


yy,zz,xx = np.meshgrid(np.linspace(0, ys-1, ys), np.linspace(0, zs-1, zs), np.linspace(0, xs-1, xs) )

# pos = np.array(zip(*[zz.flatten(), yy.flatten(), xx.flatten()]) , dtype = np.int)

pos = np.array([zz.flatten(), yy.flatten(), xx.flatten()] , dtype = np.int).reshape((3,-1)).transpose()


pos


p = []

for z in xrange(zs):
    for y in xrange(ys):
        for x in xrange(xs):
            p.append([z, y, x])

p = np.array(p, dtype=np.int)

