__author__ = 'kamal'


def imposeHeatSourceVolume(self, heat_source_id = None, Q = 1, c = 1, rho = 1, fraction = 1, \
                     mat_id = None, positionOffset= (0,0,0), \
                     heatSourceRange = (10000.0, 10000.0, 10000.0)):

    hs = MKHeatSourceData(heat_id = heat_source_id, mat_id=mat_id, Q = Q, c = c, rho = rho, fraction = fraction)

    #base_pos = self.getMaterialBasePosition(mat_id=mat_id)
    #print 'base_pos == %s', base_pos

    model_coords = self.getMaterialCoords(mat_id= mat_id)

    print 'model ID === %d coords shape === %s' % (mat_id, model_coords.shape, )

    coords_vals =  model_coords #[idx,:]
    cval = np.mean(coords_vals, axis=0) + np.array(positionOffset)

    print 'coords_vals shape', coords_vals.shape
    print 'cvals', cval

    idx = np.abs(cval[0] - coords_vals[:,0]) <  heatSourceRange[0]
    coords_vals =  coords_vals[idx,:]

    idx = np.abs(cval[1] - coords_vals[:,1]) <  heatSourceRange[1]
    coords_vals =  coords_vals[idx,:]

    idx = np.abs(cval[2] - coords_vals[:,2]) <  heatSourceRange[2]
    coords_vals =  coords_vals[idx,:]

    zs,ys,xs = self.model_objects[mat_id].model.model.shape

    pixel_volume = zs*ys*xs

    hs.coords = coords_vals # model_coords[idx,:]

    hs.surface_area_of_heat_source_plane_in_pixels = hs.coords.__len__()
    hs.volume_of_material_in_pixel = pixel_volume

    hs.H = Q/(rho*c)

    hs.H_per_pixel = (hs.H * 1.0) #/ hs.surface_area
    self.heatSource[heat_source_id] = hs

    print 'total_voxel_volume == ', hs.total_voxel_volume
    print 'surface area === ', hs.surface_area
    print 'H   == ', hs.H
    print 'H_per_pixel == ', hs.H_per_pixel
    print 'hs.coords.shape ', hs.coords.shape




# callit as

main_system.imposeHeatSourceVolume(heat_source_id = 1, Q = 1, c = 1, rho = 1, fraction = 1, \
                         mat_id = 3, positionOffset= (0,0,0), \
                         heatSourceRange = [2,5,5])
