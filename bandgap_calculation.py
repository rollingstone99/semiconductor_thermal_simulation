__author__ = 'kamal'

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt



def Eg_func(Eg0=0, T=0, alpha0=1, beta0=1):
    tk =  T + 273.0 *0.0
    return Eg0 - ( alpha0 * tk**2/(tk+beta0) )


def Eg_Silicon(E0 = 0, E_vals = (0,0,0), T = 0):
    tk = (273.0*0.0 + T)/300.0

    eg_val = E0 +  E_vals[0] * tk +  E_vals[1] * tk**2 +  E_vals[2]* tk**3
    return eg_val


Eg0_germenium = 0.7437 # eV
a_germenium = 4.7e-4 # eV/K
b_germenium = 235 # K


# Si
# Green parameters




tval = np.linspace(0, 600,50)
eg_val = Eg_func(Eg0_germenium,tval,a_germenium,b_germenium)
# eg_val = Eg_func(1,tval,a_germenium,b_germenium)
# eg_val = Eg_func(1,tval,a_germenium,b_germenium)



plt.figure(100)
plt.clf()
plt.cla()
ax = plt.subplot()

plt.plot(tval, eg_val)
plt.xlabel('Temperature (K)')
plt.ylabel('Band Gap (eV)')
plt.show()



E0_si = 1.17
E_vals = [0.00572, -0.06948, 0.018]


plt.figure(200)
plt.clf()
plt.cla()
ax = plt.subplot()

plt.plot(tval, Eg_Silicon(E0_si, E_vals, T = tval))
plt.xlabel('Temperature (K)')
plt.ylabel('Band Gap (eV)')

plt.show()












