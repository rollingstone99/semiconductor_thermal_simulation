
import numpy as np
import scipy as sp
import theano
import theano.tensor as T
from multiprocessing import Pool
import time as tm
import  numexpr as ne

# import weave

import scipy.weave as wv

def calc_heat_propagation(A):
    a0 = A[0]
    # a0 = A[0]
    z0 = A[1]
    a0[1:-1,1:-1,1:-1] = a0[2:,1:-1,1:-1] + a0[:-2,1:-1,1:-1] - 2.0 * a0[1:-1,1:-1, 1:-1]
    a0[z0] = 0
    return a0


pool = Pool(processes=8)

a = np.random.rand(10,30,30)
b = a == 0

pval = [a,b]
result0 = []
# calc_heat_propagation(a)

param0 = [[a,b] for _ in xrange(3)]



last_time = tm.time()

result0 = pool.map_async(calc_heat_propagation, param0)

while not result0.ready():
    pass

print 'Time taken == ', (tm.time() - last_time)*1000.0, ' ms'
# print result0.get()





last_time = tm.time()
for pp in param0:
    calc_heat_propagation(pp)

print 'Time taken == ', (tm.time() - last_time)*1000.0, ' ms'










sparam = [i for i in xrange(4)]
result = pool.map_async(np.sin, sparam)

while not result.ready():
    pass
# pool.close()
# pool.join()


print result.get()



pool.close()

x  =T.matrix()
rv = np.random.rand(5,7)

w = theano.shared((rv).astype(theano.config.floatX) )

y = T.dot(x,w)


f0 = theano.function([x], y)

vv = f0([[1,2,3,4,5], [1,2,3,4,5]])












z1 = np.arange(1000000)

%timeit ne.evaluate('sin(z1)')
%timeit np.sin(z1)

fl = lambda x: calc_heat_propagation(x)

a = np.random.rand(100,300,300)
dd = sp.copy(a)
b = a == 0



%timeit ne.evaluate('power(a+dd,1)')
%timeit np.power(a+dd,1)




%timeit calc_heat_propagation(pval)






def weave_update():
    code = """
    int i, j;
    for (i=1; i<Nu[0]-1; i++) {
       for (j=1; j<Nu[1]-1; j++) {
           U2(i,j) = ((U2(i+1, j) + U2(i-1, j))*dy2 + \
                       (U2(i, j+1) + U2(i, j-1))*dx2) / (2*(dx2+dy2));
       }
    }
    """
    return wv.inline(code, ['u', 'dx2', 'dy2'])





u_data = [[1,2],[3,4],[5,6]]


fw = weave_update()



def binary_search(seq, t):
    min = 0; max = len(seq) - 1
    while 1:
        if max < min:
            return -1
        m = (min  + max)  / 2
        if seq[m] < t:
            min = m  + 1
        elif seq[m] > t:
            max = m  - 1
        else:
            return m


def c_int_binary_search(seq,t):
    # do a little type checking in Python
    assert(type(t) == type(1))
    assert(type(seq) == type([]))

    # now the C code
    code = """
           #line 29 "binary_search.py"
           int val, m, min = 0;
           int max = seq.length() - 1;
           PyObject *py_val;
           for(;;)
           {
               if (max < min  )
               {
                   return_val =  Py::new_reference_to(Py::Int(-1));
                   break;
               }
               m =  (min + max) /2;
               val = py_to_int(PyList_GetItem(seq.ptr(),m),"val");
               if (val  < t)
                   min = m  + 1;
               else if (val >  t)
                   max = m - 1;
               else
               {
                   return_val = Py::new_reference_to(Py::Int(m));
                   break;
               }
           }
           """
    return wv.inline(code,['seq','t'])

sdata = [1,2,3,4,5,6,6,7,8,9,9,8,8,345,54,65,5,6]

bsearch_func = c_int_binary_search(sdata, 9)
